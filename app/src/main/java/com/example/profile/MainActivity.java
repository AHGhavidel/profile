package com.example.profile;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private User user;

    private Button btn_Save_Info, btn_Edit_Profile, btn_View_Web;
    private EditText et_About_Me;
    private TextView tv_Full_name;
    private CheckBox chb_Android, chb_Ui, chb_Deep;
    private RadioGroup rgp_gender;
    private String gender, str_About_Me;

    private boolean android, ui, deep;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Button TextView ...

        user = new User(this);

        btn_Save_Info = findViewById(R.id.btn_main_saveInformation);
        btn_Edit_Profile = findViewById(R.id.btn_main_editProfile);
        btn_View_Web = findViewById(R.id.btn_main_viewWebsite);

        tv_Full_name = findViewById(R.id.tv_main_full_name);

        chb_Android = findViewById(R.id.checkBox_main_android);
        chb_Ui = findViewById(R.id.checkbox_main_ui);
        chb_Deep = findViewById(R.id.checkBox_main_deepLearning);

        rgp_gender = findViewById(R.id.radioGroup_main);

        et_About_Me = findViewById(R.id.et_main_about_me);

        btn_Edit_Profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn_View_Web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        chb_Android.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    android = true;
                }
                else{
                    android = false;
                }
            }
        });

        chb_Ui.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    ui = true;
                }
                else{
                    ui = false;
                }
            }
        });

        chb_Deep.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    deep = true;
                }
                else{
                    deep = false;
                }
            }
        });

        rgp_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int isChecked) {
                if(isChecked == R.id.radioButton_main_male){
                    gender = "male";
                }
                else{
                    gender = "feMale";
                }
            }
        });

        btn_Save_Info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                str_About_Me = et_About_Me.getText().toString();
                user.saveUserInformation(android, ui, deep, gender,"Me", str_About_Me, "www.google.com");
            }
        });
    }
}