package com.example.profile;

import android.content.Context;
import android.content.SharedPreferences;

public class User {
    private SharedPreferences sharedPreferences;
    public User(Context context) {
        sharedPreferences = context.getSharedPreferences("profile", context.MODE_PRIVATE);
    }

    SharedPreferences.Editor editor = sharedPreferences.edit();

    public void saveUserInformation(boolean android, boolean ui, boolean deep, String gender, String fullName, String about_me, String webSite){

    editor.putBoolean("android", android);
    editor.putBoolean("ui", ui);
    editor.putBoolean("deep", deep);
    editor.putString("gender", gender);
    editor.putString("fullName", fullName);
    editor.putString("about Me", about_me);
    editor.putString("webSite", webSite);

    }

    public boolean getAndroid(){
        return sharedPreferences.getBoolean("android", false);
    }

    public boolean getUi(){
        return sharedPreferences.getBoolean("ui", false);
    }

    public boolean getDeep(){
        return sharedPreferences.getBoolean("deep", false);
    }

    public String getGender(){
        return sharedPreferences.getString("gender", "male");
    }

    public String getFullName(){
        return sharedPreferences.getString("fullName", "Me");
    }

    public void setFullName(String fullName){
        editor.putString("fullName",fullName);
    }

    public String getAbout_Me(){
        return sharedPreferences.getString("about Me", " ");
    }

    public String getWebSite(){
        return sharedPreferences.getString("webSite", "www.google.com");
    }

    public void setWebSite(String webSite){
        editor.putString("webSite", webSite);
    }

}
